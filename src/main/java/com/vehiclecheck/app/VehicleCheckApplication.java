package com.vehiclecheck.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VehicleCheckApplication {

	public static void main(String[] args) {
		SpringApplication.run(VehicleCheckApplication.class, args);
	}

}
