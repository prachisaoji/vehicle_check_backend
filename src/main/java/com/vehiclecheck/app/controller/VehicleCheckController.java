package com.vehiclecheck.app.controller;

import java.io.EOFException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.vehiclecheck.app.model.VehicleApiModel;
import com.vehiclecheck.app.model.VehicleApiResponseResult;
import com.vehiclecheck.app.util.VehicleApiRequest;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class VehicleCheckController {

	Logger logger = Logger.getLogger(VehicleCheckController.class.getName());
	@Autowired
	VehicleApiRequest vehicleApiRequest;

	@RequestMapping(value = "/getVehicleApiResponse", method = RequestMethod.POST)
	public VehicleApiResponseResult getVehicleApiResponse(@RequestBody VehicleApiModel vehicleApiModel,
			RedirectAttributes redirectAttrs)
			throws EOFException, ClassNotFoundException, NoSuchAlgorithmException, IOException {

		logger.info("In getVehicleApiResponse() for vehicleVinOrRegistrationNumber " + vehicleApiModel.getVinAndRegNo()
				+ "  and , vehicle guide month " + vehicleApiModel.getGuideMonth() + " , vehicle guide year "
				+ vehicleApiModel.getGuideYear() + ", VehicleMileage " + vehicleApiModel.getVehicleMileage());

		String vehicleResponse = "";
		VehicleApiResponseResult vehicleApiResponseResult = new VehicleApiResponseResult();

		try {

			vehicleApiModel.setGuideMonth("1");
			vehicleApiModel.setGuideYear("2020");
			vehicleApiModel.setVehicleMileage("0");
			// okhttp vehicle API request call
			vehicleResponse = vehicleApiRequest.getConvergedDataRequest(vehicleApiModel.getVinAndRegNo(),
					vehicleApiModel.getGuideMonth(), vehicleApiModel.getGuideYear(),
					vehicleApiModel.getVehicleMileage());
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("vehicleResponse : " + vehicleResponse);
		try {
			// parsing the elements from XML response
			vehicleApiResponseResult = vehicleApiRequest.parseGetConvergedDataRequestXMLResponse(vehicleResponse,
					vehicleApiModel.getVinAndRegNo());
		} catch (Exception e) {
			logger.info("exception " + e);

		}

		return vehicleApiResponseResult;

	}
}
