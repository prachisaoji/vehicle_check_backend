package com.vehiclecheck.app.model;

public class VehicleApiResponseResult {

	private String vehicleMmCode;
	private String vehicleMake;
	private String vehicleModel;
	private String vehicleDerivative;
	private String vehicleIntroDate;
	private String vehicleDiscDate;
	private String vehicleColour;
	private String vehicleRegistrationNumber;
	private String vehicleVinNumber;
	private String vehicleexpiryDate;
	private String vehicleAxleConfigration;
	private String vehiclebodyType;
	private String vehicleCubicCapacity;
	private String vehicleDoors;
	private String vehicleFrontTireSize;
	private String vehcileGCM;
	private String vehicleGVM;
	private String vehicleKilowatts;
	private String vehicleNoOfCylinder;
	private String vehicleRearTyreSize;
	private String vehicleWheelBase;
	private String vehicleTare;
	
	public String getVehicleMmCode() {
		return vehicleMmCode;
	}
	public void setVehicleMmCode(String vehicleMmCode) {
		this.vehicleMmCode = vehicleMmCode;
	}
	public String getVehicleMake() {
		return vehicleMake;
	}
	public void setVehicleMake(String vehicleMake) {
		this.vehicleMake = vehicleMake;
	}
	public String getVehicleModel() {
		return vehicleModel;
	}
	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}
	public String getVehicleDerivative() {
		return vehicleDerivative;
	}
	public void setVehicleDerivative(String vehicleDerivative) {
		this.vehicleDerivative = vehicleDerivative;
	}
	public String getVehicleIntroDate() {
		return vehicleIntroDate;
	}
	public void setVehicleIntroDate(String vehicleIntroDate) {
		this.vehicleIntroDate = vehicleIntroDate;
	}
	public String getVehicleDiscDate() {
		return vehicleDiscDate;
	}
	public void setVehicleDiscDate(String vehicleDiscDate) {
		this.vehicleDiscDate = vehicleDiscDate;
	}
	public String getVehicleColour() {
		return vehicleColour;
	}
	public void setVehicleColour(String vehicleColour) {
		this.vehicleColour = vehicleColour;
	}
	public String getVehicleRegistrationNumber() {
		return vehicleRegistrationNumber;
	}
	public void setVehicleRegistrationNumber(String vehicleRegistrationNumber) {
		this.vehicleRegistrationNumber = vehicleRegistrationNumber;
	}
	public String getVehicleVinNumber() {
		return vehicleVinNumber;
	}
	public void setVehicleVinNumber(String vehicleVinNumber) {
		this.vehicleVinNumber = vehicleVinNumber;
	}
	public String getVehicleexpiryDate() {
		return vehicleexpiryDate;
	}
	public void setVehicleexpiryDate(String vehicleexpiryDate) {
		this.vehicleexpiryDate = vehicleexpiryDate;
	}
	public String getVehicleAxleConfigration() {
		return vehicleAxleConfigration;
	}
	public void setVehicleAxleConfigration(String vehicleAxleConfigration) {
		this.vehicleAxleConfigration = vehicleAxleConfigration;
	}
	public String getVehiclebodyType() {
		return vehiclebodyType;
	}
	public void setVehiclebodyType(String vehiclebodyType) {
		this.vehiclebodyType = vehiclebodyType;
	}
	public String getVehicleCubicCapacity() {
		return vehicleCubicCapacity;
	}
	public void setVehicleCubicCapacity(String vehicleCubicCapacity) {
		this.vehicleCubicCapacity = vehicleCubicCapacity;
	}
	public String getVehicleDoors() {
		return vehicleDoors;
	}
	public void setVehicleDoors(String vehicleDoors) {
		this.vehicleDoors = vehicleDoors;
	}
	public String getVehicleFrontTireSize() {
		return vehicleFrontTireSize;
	}
	public void setVehicleFrontTireSize(String vehicleFrontTireSize) {
		this.vehicleFrontTireSize = vehicleFrontTireSize;
	}
	public String getVehcileGCM() {
		return vehcileGCM;
	}
	public void setVehcileGCM(String vehcileGCM) {
		this.vehcileGCM = vehcileGCM;
	}
	public String getVehicleGVM() {
		return vehicleGVM;
	}
	public void setVehicleGVM(String vehicleGVM) {
		this.vehicleGVM = vehicleGVM;
	}
	public String getVehicleKilowatts() {
		return vehicleKilowatts;
	}
	public void setVehicleKilowatts(String vehicleKilowatts) {
		this.vehicleKilowatts = vehicleKilowatts;
	}
	public String getVehicleNoOfCylinder() {
		return vehicleNoOfCylinder;
	}
	public void setVehicleNoOfCylinder(String vehicleNoOfCylinder) {
		this.vehicleNoOfCylinder = vehicleNoOfCylinder;
	}
	public String getVehicleRearTyreSize() {
		return vehicleRearTyreSize;
	}
	public void setVehicleRearTyreSize(String vehicleRearTyreSize) {
		this.vehicleRearTyreSize = vehicleRearTyreSize;
	}
	public String getVehicleWheelBase() {
		return vehicleWheelBase;
	}
	public void setVehicleWheelBase(String vehicleWheelBase) {
		this.vehicleWheelBase = vehicleWheelBase;
	}
	public String getVehicleTare() {
		return vehicleTare;
	}
	public void setVehicleTare(String vehicleTare) {
		this.vehicleTare = vehicleTare;
	}
	
	
	
}
