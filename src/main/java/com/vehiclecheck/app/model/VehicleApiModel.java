package com.vehiclecheck.app.model;

public class VehicleApiModel {

	private String vehicleRegistrationNumebr;
	private String vehicleVinNumber;
	private String guideMonth;
	private String guideYear;
	private String vehicleMileage;
	private String vinAndRegNo;
	
	public String getVehicleRegistrationNumebr() {
		return vehicleRegistrationNumebr;
	}
	public void setVehicleRegistrationNumebr(String vehicleRegistrationNumebr) {
		this.vehicleRegistrationNumebr = vehicleRegistrationNumebr;
	}
	public String getVehicleVinNumber() {
		return vehicleVinNumber;
	}
	public void setVehicleVinNumber(String vehicleVinNumber) {
		this.vehicleVinNumber = vehicleVinNumber;
	}
	public String getGuideMonth() {
		return guideMonth;
	}
	public void setGuideMonth(String guideMonth) {
		this.guideMonth = guideMonth;
	}
	public String getGuideYear() {
		return guideYear;
	}
	public void setGuideYear(String guideYear) {
		this.guideYear = guideYear;
	}
	public String getVehicleMileage() {
		return vehicleMileage;
	}
	public void setVehicleMileage(String vehicleMileage) {
		this.vehicleMileage = vehicleMileage;
	}
	public String getVinAndRegNo() {
		return vinAndRegNo;
	}
	public void setVinAndRegNo(String vinAndRegNo) {
		this.vinAndRegNo = vinAndRegNo;
	}
	
	
}
