package com.vehiclecheck.app.util;

import java.io.EOFException;

import java.util.Date;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class Util {
	static Logger logger = Logger.getLogger(Util.class.getName());

	public Properties readPropertiesFile(String propertiesFilePath)
			throws EOFException, IOException, ClassNotFoundException {
		Properties properties = null;
		// logger.info(" propertiesFilePath in readPropertiesFile() : " +
		// propertiesFilePath);

		try {
			properties = new Properties();
			InputStream inputStream = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(propertiesFilePath);
			properties.load(inputStream);
		} catch (Exception e) {
			// TODO: handle exception
			// logger.info("Exception" + e);
			e.printStackTrace();

		}

		return properties;

	}

	// method to convert string date to formated date
	public static String convertDateIntoFormatedDate(String stringDate) {
		DateFormat sourceDateFormat;
		DateFormat destDateFormat;
		String formatedDate = "";
		logger.info(" convertDateIntoFormatedDate() method : " + stringDate);

		try {
			sourceDateFormat = new SimpleDateFormat("yyyyMMdd");
			Date date = sourceDateFormat.parse(stringDate);
			destDateFormat = new SimpleDateFormat("dd MMM yyyy");
			formatedDate = destDateFormat.format(date);

		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception in convertDateIntoFormatedDate() method :", e);

		}
		return formatedDate;
	}

}
