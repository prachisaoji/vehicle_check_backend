package com.vehiclecheck.app.util;

import java.io.EOFException;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.lang.model.element.Element;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.events.Namespace;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.gson.Gson;
import com.vehiclecheck.app.model.VehicleApiResponseResult;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Component
public class VehicleApiRequest {

	static Logger logger = Logger.getLogger(VehicleApiRequest.class.getName());
	/*
	 * static String GuideMonth = "5"; static String GuideYear = "2020"; static
	 * String VehManufYear = "2015"; static String VehMMCode = "02045100"; static
	 * String VehMileage = "100000"; static String reqPerson = ""; static String
	 * ClientReference = "testService";
	 */

	// static String GuideMonth = "10";
	// static String GuideYear = "2020";
	// static String VehManufYear = "2010";
	// static String VehMMCode = "02045100";
	// static String VehMileage = "0";
	// static String reqPerson = "Transunion";
	// static String ClientReference = "Transunion";
	// static String VehicleRegistrationNumber = "BB72TMGP";
	// static String VehicleVinNumber = "ZAR94000007037279";
	// static String Vehicle1EnginNumber = "64698251411057";

	public String getConvergedDataRequest(String vinAndRegNo, String guideMonth, String guideYear,
			String vehicleMileage) throws EOFException, ClassNotFoundException, IOException, NoSuchAlgorithmException,
			InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {

		String xmlInput = "";

		Util util = new Util();
		Properties prop = util.readPropertiesFile("vehicleapicredentials.properties");
		String apiKey = prop.getProperty("ApiKey");
		String reportCode = prop.getProperty("ReportCode");
		String subscriptionUsername = prop.getProperty("SubscriptionUsername");
		String subscriptionPassword = prop.getProperty("SubscriptionPassword");
		String apiurl = prop.getProperty("Apiurl");
		String reqPerson = prop.getProperty("reqPerson");
		String clientReference = prop.getProperty("ClientReference");

		Gson gsonUtiltiy = new Gson();

		String genrateHash = generateHash(clientReference, reqPerson, vinAndRegNo, guideMonth, guideYear,
				vehicleMileage, subscriptionUsername, subscriptionPassword);
		String soapString = appendSoapValues(vinAndRegNo, guideMonth, guideYear, vehicleMileage, reqPerson,
				clientReference, subscriptionUsername, subscriptionPassword, apiKey, reportCode);

		logger.info(" In  getConvergedDataRequest() method   ");

		OkHttpClient client = new OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS) // connect timeout
				.writeTimeout(30, TimeUnit.SECONDS) // write timeout
				.readTimeout(30, TimeUnit.SECONDS) // read timeout
				.build();

		MediaType mediaType = MediaType.parse("xml");

		RequestBody body = RequestBody.create(mediaType, soapString);
		Request requestForPartSearch = new Request.Builder().url(apiurl).addHeader("request-hash", genrateHash)
				.addHeader("content-type", "application/soap+xml;charset=\"utf-8\"").post(body).build();

		Response response = null;

		try {
			response = client.newCall(requestForPartSearch).execute();
			// System.out.println(response.body().string());
			System.out.println(gsonUtiltiy.toJson(" Response : " + response));
			xmlInput = response.body().string(); // <-- The XML SOAP response

		}

		catch (EOFException eof) {
			eof.printStackTrace();
			System.out.println(eof);
		} catch (IOException ie) {
			ie.printStackTrace();

			System.out.println(ie);
		} catch (Exception e) {
			e.printStackTrace();

			System.out.println(e);
		}

		return xmlInput;
	}

	public String generateHash(String clientReference, String reqPerson, String vinAndRegNo, String guideMonth,
			String guideYear, String vehicleMileage, String subscriptionUsername, String subscriptionPassword)
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException,
			BadPaddingException {

		String SaltKey_Test = "TrUnCl!entH@sh";
		String Username_Test = "195187";
		String UsrPwd_Test = "NV@etVal!87";
		// String Username_Test = "198564";
		// String UsrPwd_Test = "TEST@6789#";

		logger.info(" In generateHash() method");

		String objtohash = clientReference + guideMonth + guideYear + reqPerson + subscriptionPassword
				+ subscriptionUsername + vehicleMileage + vinAndRegNo + SaltKey_Test;

		// var objtohash = ClientReference + GuideMonth + GuideYear + reqPerson +
		// UsrPwd_Test + Username_Test + VehManufYear + VehMileage + VehMMCode +
		// SaltKey_Test;
		logger.info("concatenated String are : " + objtohash);

		byte[] asciiValue = objtohash.getBytes(StandardCharsets.US_ASCII);

		logger.info("converted ASCII value are : " + asciiValue);

		MessageDigest digest = MessageDigest.getInstance("SHA-1");

		byte[] sha1Bytes = digest.digest(asciiValue);

		logger.info("converted SHA1 value are : " + sha1Bytes.toString());

		String encodedString = Base64.getEncoder().encodeToString(sha1Bytes);

		logger.info("genrated hash are : " + encodedString);

		return encodedString.toString();
	}

	private static String appendSoapValues(String vinAndRegNo, String guideMonth, String guideYear, String vehMileage,
			String reqPerson, String ClientReference, String subscriptionUsername, String subscriptionPassword,
			String apiKey, String reportCode) {
		StringBuilder soapenvelop = new StringBuilder();

		logger.info(" In  appendSoapValues() method ");

		soapenvelop.append(
				"<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:typ=\"http://autoinsight.transunion.co.za/types\">");
		soapenvelop.append("<soap:Header/>");
		soapenvelop.append("<soap:Body>");
		soapenvelop.append(
				"<typ:GetConvergedDataRequest xmlns=\"http://autoinsight.transunion.co.za/types\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">");
		soapenvelop.append("<typ:ApiKey>" + apiKey + "</typ:ApiKey>");
		soapenvelop.append("<typ:ReportCode>" + reportCode + "</typ:ReportCode>");
		soapenvelop.append("<typ:Input i:type=\"HPIReportRequest\">");
		soapenvelop.append("<typ:SubscriptionUsername>" + subscriptionUsername + "</typ:SubscriptionUsername>");
		soapenvelop.append("<typ:SubscriptionPassword>" + subscriptionPassword + "</typ:SubscriptionPassword>");

		// if it is VehicleVinNumber
		if (vinAndRegNo.length() == 17) {
			soapenvelop.append("<typ:VehicleVinNumber>" + vinAndRegNo + "</typ:VehicleVinNumber>");
		} else {
			soapenvelop.append("<typ:VehicleVinNumber/>");

		}
		soapenvelop.append("<typ:VehicleColour/>");
		soapenvelop.append("<typ:VehicleMMCode/>");
		// soapenvelop.append("<typ:VehicleMMCode>"+ VehMMCode +"</typ:VehicleMMCode>");
		// soapenvelop.append("<typ:VehicleEngineNumber>" + Vehicle1EnginNumber +
		// "</typ:VehicleEngineNumber>");
		soapenvelop.append("<typ:VehicleEngineNumber/>");
		// soapenvelop.append("<typ:VehicleManufactureYear>" + VehManufYear +
		// "</typ:VehicleManufactureYear>");
		soapenvelop.append("<typ:VehicleManufactureYear/>");
		soapenvelop.append("<typ:ClientReference>" + ClientReference + "</typ:ClientReference>");
		soapenvelop.append("<typ:ConsumerCellularNumber/>");
		soapenvelop.append("<typ:RequestorPerson>" + reqPerson + "</typ:RequestorPerson>");
		// if it is VehicleRegistrationNumber
		if (vinAndRegNo.length() != 17) {
			soapenvelop.append("<typ:VehicleRegistrationNumber>" + vinAndRegNo + "</typ:VehicleRegistrationNumber>");
		} else {
			soapenvelop.append("<typ:VehicleRegistrationNumber/>");

		}

		// soapenvelop.append("<typ:ManufactureCode/>");
		soapenvelop.append("<typ:GuideYear>" + guideYear + "</typ:GuideYear>");
		soapenvelop.append(
				"<typ:GuideMonth>" + String.format("%010d", Integer.parseInt(guideMonth)) + "</typ:GuideMonth>");
		// soapenvelop.append("<typ:GuideMonth>" + guideMonth + "</typ:GuideMonth>");
		soapenvelop.append("<typ:Condition/>");
		soapenvelop.append("<typ:VehicleMileage>" + vehMileage + "</typ:VehicleMileage>");
		soapenvelop.append("<typ:OptionCodeUsed/>");
		soapenvelop.append("<typ:OptionCodeNew/>");
		soapenvelop.append("</typ:Input>");
		soapenvelop.append("</typ:GetConvergedDataRequest>");
		soapenvelop.append("</soap:Body>");
		soapenvelop.append("</soap:Envelope>");

		System.out.println("soapenvelop : " + soapenvelop);
		return soapenvelop.toString();
	}

	public VehicleApiResponseResult parseGetConvergedDataRequestXMLResponse(String xmlResponse,
			String vehicleVinOrRegistrationNumber) {

		logger.info(" In  parseGetConvergedDataRequestXMLResponse() method ");
		logger.info("xmlResponse :" + xmlResponse);
		DocumentBuilder builder = null;
		VehicleApiResponseResult vehicleApiResponseResult = new VehicleApiResponseResult();
		logger.info("In parseGetConvergedDataRequestXMLResponse()");
		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException pce) {
			// TODO Auto-generated catch block
			pce.printStackTrace();
			logger.info("ParserConfigurationException" + pce);
		}
		InputSource src = new InputSource();
		src.setCharacterStream(new StringReader(xmlResponse));

		org.w3c.dom.Document doc = null;
		try {
			doc = builder.parse(src);
		} catch (SAXException saxe) {
			// TODO Auto-generated catch block
			logger.info("SAXException" + saxe);
			saxe.printStackTrace();
		} catch (IOException ioe) {
			// TODO Auto-generated catch block
			logger.info("IOException" + ioe);
			ioe.printStackTrace();
		}
		try {
			// ResultCodeDescription
			NodeList errorCodeNode = doc.getElementsByTagName("d4p1:ErrorCode");

			int iferrorCodePresentIntheXml = errorCodeNode.item(0).getChildNodes().getLength();

			if (iferrorCodePresentIntheXml == 0) {

				NodeList resultCodeDescriptionNode = doc.getElementsByTagName("d4p1:ResultCodeDescription");

				int isResultCodeDescIsExistInXml = resultCodeDescriptionNode.item(0).getChildNodes().getLength();

				// if vehicle data not fount in transunion database

				if (isResultCodeDescIsExistInXml == 0) {

					// if entered number is vehicle vin number
					if (vehicleVinOrRegistrationNumber.length() == 17) {
						vehicleApiResponseResult.setVehicleVinNumber(vehicleVinOrRegistrationNumber);
					} else {// else if entered number is vehicleRegistrationNumber
						vehicleApiResponseResult.setVehicleRegistrationNumber(vehicleVinOrRegistrationNumber);
					}

					vehicleApiResponseResult
							.setVehicleMake(doc.getElementsByTagName("d4p1:VehicleMake").item(0).getTextContent());
					vehicleApiResponseResult
							.setVehicleModel(doc.getElementsByTagName("d4p1:VehicleModel").item(0).getTextContent());

					// Extracting date from information full date format
					String informationDate = doc.getElementsByTagName("d4p1:IntroductionDate").item(0).getTextContent();
					vehicleApiResponseResult
							.setVehicleIntroDate(informationDate.substring(0, informationDate.indexOf("T")));

					String discountDate = doc.getElementsByTagName("d4p1:DiscontinuedDate").item(0).getTextContent();

					vehicleApiResponseResult.setVehicleDiscDate(discountDate.substring(0, discountDate.indexOf("T")));

					vehicleApiResponseResult.setVehicleAxleConfigration(
							doc.getElementsByTagName("d4p1:AxleConfiguration").item(0).getTextContent());
					vehicleApiResponseResult
							.setVehiclebodyType(doc.getElementsByTagName("d4p1:BodyType").item(0).getTextContent());
					vehicleApiResponseResult.setVehicleCubicCapacity(
							doc.getElementsByTagName("d4p1:BodyType").item(0).getTextContent());
					vehicleApiResponseResult
							.setVehicleCubicCapacity(doc.getElementsByTagName("d4p1:CC").item(0).getTextContent());
					vehicleApiResponseResult
							.setVehicleDoors(doc.getElementsByTagName("d4p1:Doors").item(0).getTextContent());
					vehicleApiResponseResult.setVehicleFrontTireSize(
							doc.getElementsByTagName("d4p1:FrontTyreSize").item(0).getTextContent());
					vehicleApiResponseResult
							.setVehcileGCM(doc.getElementsByTagName("d4p1:GCM").item(0).getTextContent());
					vehicleApiResponseResult
							.setVehicleGVM(doc.getElementsByTagName("d4p1:GVM").item(0).getTextContent());
					vehicleApiResponseResult
							.setVehicleKilowatts(doc.getElementsByTagName("d4p1:Kilowatts").item(0).getTextContent());
					vehicleApiResponseResult.setVehicleNoOfCylinder(
							doc.getElementsByTagName("d4p1:Cylinders").item(0).getTextContent());
					vehicleApiResponseResult.setVehicleRearTyreSize(
							doc.getElementsByTagName("d4p1:RearTyreSize").item(0).getTextContent());
					vehicleApiResponseResult
							.setVehicleTare(doc.getElementsByTagName("d4p1:TareMass").item(0).getTextContent());
					vehicleApiResponseResult
							.setVehicleWheelBase(doc.getElementsByTagName("d4p1:WheelBase").item(0).getTextContent());
				}
			} else {
				// error code is getting
				System.out.println("error code is  xml tag getting in the xml response");
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

		return vehicleApiResponseResult;

	}

}
